package com.bsa.config;

import com.bsa.dto.GifDto;
import com.bsa.services.GifConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
@Configuration
public class JsonConfig {
    @Bean
    public Gson gson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(GifDto.class, new GifConverter());
        return builder.create();
    }
}
