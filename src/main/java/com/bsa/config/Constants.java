package com.bsa.config;

import java.util.List;

import static java.io.File.separator;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 06.07.2020
 */
public class Constants {
    public static final String UPLOAD_DIRECTORY = "bsa_giphy";
    public static final String CACHE_DIRECTORY = UPLOAD_DIRECTORY + separator + "cash";
    public static final String USERS_DIRECTORY = UPLOAD_DIRECTORY + separator + "users";

    public static final List<String> ILLEGAL_CHARACTERS = List.of("/", "\\", "`", "?", "*", "<", ">", "|", "\"", ":");
}
