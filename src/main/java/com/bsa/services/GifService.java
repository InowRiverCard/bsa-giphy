package com.bsa.services;

import com.bsa.dto.GifDto;
import com.bsa.dto.GifsByQueryDto;
import com.bsa.dto.HistoryDto;
import com.bsa.repository.CacheRepository;
import com.bsa.repository.FSRepository;
import com.bsa.utils.CSVReader;
import com.bsa.utils.HistoryLogger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 06.07.2020
 */
@Service
public class GifService {
    private final FSRepository repository;

    private final GifProvider provider;

    private final HistoryLogger logger;

    private final CacheRepository cacheRepository;

    private final CSVReader csvReader;

    public GifService(FSRepository repository, GifProvider provider, HistoryLogger logger,
                      CacheRepository cacheRepository, CSVReader reader) {
        this.repository = repository;
        this.provider = provider;
        this.logger = logger;
        this.cacheRepository = cacheRepository;
        this.csvReader = reader;
    }

    public String saveToCache(GifDto gif, String query) throws IOException {
        return repository.saveToCache(gif, query);
    }

    public String saveToUsers(GifDto gif, String userId, String query) throws IOException {
        var rst = repository.saveToUsers(gif, userId, query);
        logger.log(query, userId, rst);
        return rst;
    }

    public List<String> getUrlsFromFSCacheByQuery(String query) {
        return repository.getFromCashByQuery(query);
    }

    public List<String> getURLsFromUserByQuery(String userId, String query) {
        return repository.getUserURLsByQuery(userId, query);
    }

    public String putGifToFSCacheIfAbsent(GifDto gif, String query) throws IOException {
        var gifs = this.getUrlsFromFSCacheByQuery(query);

        return this.findGif(gifs, gif.getName())
                .orElse(this.saveToCache(gif, query));
    }

    public String putGifToUserIfAbsent(GifDto gif, String userId, String query) throws IOException {
        var gifs = this.getURLsFromUserByQuery(userId, query);
        return this.findGif(gifs, gif.getName())
                .orElse(this.saveToUsers(gif, userId, query));
    }

    private Optional<String> findGif(List<String> gifs, String gifName) {
        Optional<String> result = Optional.empty();
        for (String gifURL : gifs) {
            var name = gifURL.substring(gifURL.lastIndexOf("/") + 1);
            if (gifName.equals(name)) {
                result = Optional.of(gifURL);
                break;
            }
        }
        return result;
    }

    public String forceGifCreate(String userId, String query) throws IOException {
        var gif = provider.getGif(query);
        putGifToFSCacheIfAbsent(gif, query);
        return putGifToUserIfAbsent(gif, userId, query);
    }

    public String createUserGif(String userId, String query) throws IOException {
        var gifList = this.getUrlsFromFSCacheByQuery(query);
        return gifList.isEmpty()
                ? this.forceGifCreate(userId, query)
                : gifList.get(this.getRandomIndex(gifList.size()));

    }

    public Optional<String> searchGif(String userId, String query) {
        var url = this.getUserGifByQueryFromCache(userId, query);
        return url.isPresent()
                ? url
                : this.getUserGifFromFS(userId, query);
    }

    public Optional<String> forceSearchGif(String userId, String query) {
        return this.getUserGifFromFS(userId, query);
    }

    private Optional<String> getUserGifFromFS(String userId, String query) {
        Optional<String> result = Optional.empty();
        var gifs = this.repository.getUserURLsByQuery(userId, query);
        if (!gifs.isEmpty()) {
            var gifUrl = gifs.get(this.getRandomIndex(gifs.size()));
            result = Optional.of(gifUrl);
            cacheRepository.putToHash(userId, query, gifUrl);
        }
        return result;
    }

    private Optional<String> getUserGifByQueryFromCache(String userId, String query) {
        var urls = cacheRepository.getGifsByQuery(userId, query);
        return urls.isEmpty()
                ? Optional.empty()
                : Optional.of(urls.get(this.getRandomIndex(urls.size())));

    }

    private int getRandomIndex(int size) {
        return ThreadLocalRandom.current().nextInt(0, size);
    }


    public List<GifsByQueryDto> getAllFSCacheGifs() {
        return repository.getAllFSCashQueries().stream()
                .map(query -> new GifsByQueryDto(query, this.getUrlsFromFSCacheByQuery(query)))
                .collect(Collectors.toList());
    }

    public List<String> getAllGifUrls() {
        var result = new ArrayList<String>();
        result.addAll(this.getAllFSCacheGifURLs());
        result.addAll(this.getUsersGifURLs());
        return result;
    }

    private List<String> getAllFSCacheGifURLs() {
        return repository.getAllFSCashQueries().stream()
                .flatMap(query -> this.getUrlsFromFSCacheByQuery(query).stream())
                .collect(Collectors.toList());
    }

    private List<String> getUsersGifURLs() {
        List<String> result = new ArrayList<>();
        var userList = repository.getUserList();
        for (String userId : userList) {
            var userUrls = repository.getUserQueries(userId).stream()
                    .flatMap(q -> repository.getUserURLsByQuery(userId, q).stream())
                    .collect(Collectors.toList());
            result.addAll(userUrls);
        }
        return result;
    }

    public List<GifsByQueryDto> getUserGifs(String userId){
        return repository.getUserQueries(userId).stream()
                .map(query -> new GifsByQueryDto(query, this.getURLsFromUserByQuery(userId, query)))
                .collect(Collectors.toList());
    }

    public GifsByQueryDto createGifFSCache(String query) throws IOException {
        var gif = provider.getGif(query);
        this.putGifToFSCacheIfAbsent(gif, query);
        var gifs = this.getUrlsFromFSCacheByQuery(query);
        return new GifsByQueryDto(query, gifs);
    }

    public void removeFSCash() {
        repository.removeCache();
    }

    public List<HistoryDto> getUserHistory(String userId) {
        var path = repository.getUserHistoryFile(userId);
        return path.isPresent()
                ? csvReader.readFile(path.get())
                : new ArrayList<>();
    }

    public void clearUserHistory(String userId) {
        repository.clearUserHistory(userId);
    }

    public void clearUserCache(String userId) {
        cacheRepository.clear(userId);
    }

    public void clearUserCache(String userId, String query) {
        cacheRepository.clear(userId, query);
    }

    public void deleteUserData(String userId) {
        repository.deleteUserData(userId);
        cacheRepository.clear(userId);
    }
}
