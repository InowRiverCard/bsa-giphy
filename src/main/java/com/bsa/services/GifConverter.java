package com.bsa.services;

import com.bsa.dto.GifDto;
import com.google.gson.*;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.net.URL;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 05.07.2020
 */
@Service
public class GifConverter implements JsonDeserializer<GifDto> {

    @Override
    public GifDto deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
        var object = json.getAsJsonObject();
        var id = object.getAsJsonObject("data")
                .getAsJsonPrimitive("id")
                .getAsString();

        var name = id + ".gif";
        var url = this.generateUrl(id);

        return new GifDto(name, url);
    }

    @SneakyThrows
    private URL generateUrl(String id) {
        var uri = "https://i.giphy.com/media/" + id + "/giphy.gif";
        return new URL(uri);
    }

}
