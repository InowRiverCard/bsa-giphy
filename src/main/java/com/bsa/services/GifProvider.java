package com.bsa.services;

import com.bsa.dto.GifDto;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 05.07.2020
 */
@Service
public class GifProvider {
    @Value("${GIPHY_APP_KEY}")
    private String APP_KEY;

    @Value("${GIPHY_URL}")
    private String GIPHY_URL;

    private final Gson converter;

    public GifProvider(Gson converter) {
        this.converter = converter;
    }

    public GifDto getGif(String query) {
        var uri = this.getGifFetchUri(query);
        var gifInfo = this.fetchGif(uri);

        return this.converter.fromJson(gifInfo, GifDto.class);
    }

    private String fetchGif(String uri) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(uri, HttpMethod.GET, entity, String.class).getBody();
    }

    private String getGifFetchUri(String query) {
        return UriComponentsBuilder.fromHttpUrl(GIPHY_URL)
                .queryParam("api_key", APP_KEY)
                .queryParam("s", query)
                .toUriString();
    }

}
