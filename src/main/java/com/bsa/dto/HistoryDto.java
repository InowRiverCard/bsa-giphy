package com.bsa.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
@AllArgsConstructor
@Data
public class HistoryDto {
    private String date;
    private String query;
    private String gif;
}
