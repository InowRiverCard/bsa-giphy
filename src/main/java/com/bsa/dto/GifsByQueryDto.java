package com.bsa.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 06.07.2020
 */
@AllArgsConstructor
@Data
public class GifsByQueryDto {
    private String query;
    private List<String> gifs;
}
