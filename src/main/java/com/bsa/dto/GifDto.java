package com.bsa.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 05.07.2020
 */
@Data
@AllArgsConstructor
public class GifDto {
    private String name;
    private URL url;
}
