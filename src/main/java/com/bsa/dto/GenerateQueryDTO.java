package com.bsa.dto;

import lombok.Data;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 05.07.2020
 */
@Data
public class GenerateQueryDTO {
    private String query;
    private boolean force;
}
