package com.bsa.controllers;

import com.bsa.dto.GenerateQueryDTO;
import com.bsa.dto.GifsByQueryDto;
import com.bsa.dto.HistoryDto;
import com.bsa.exeption.ValidationException;
import com.bsa.services.GifService;
import com.bsa.validation.FolderNameValidator;
import com.bsa.validation.ValidId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 06.07.2020
 */
@Validated
@RestController
public class ApiController {

    final GifService service;

    private final FolderNameValidator validator;

    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    public ApiController(GifService service, FolderNameValidator validator) {
        this.service = service;
        this.validator = validator;
    }

    @PostMapping(value = "/user/{id}/generate")
    public String generateGif(GenerateQueryDTO generateDTO,
                              @PathVariable @ValidId String id)
            throws IOException {
        var query = generateDTO.getQuery();
        var errors = validator.validate(query);
        if (errors.isPresent()) {
            throw new ValidationException("query: " + errors.get());
        }
        logger.info(String.format("generateGif user=%s, query=%s, isForce=%s", id, query, generateDTO.isForce()));
        var result = generateDTO.isForce()
                ? service.forceGifCreate(id, query)
                : service.createUserGif(id, query);
        logger.info("generated gif=" + result);
        return result;
    }

    @GetMapping(value = "/user/{id}/search")
    public ResponseEntity<String> searchGifFromUser(@PathVariable @ValidId String id,
                                                    @RequestParam(value = "force", required = false) boolean isForce,
                                                    @RequestParam String query) {
        logger.info(String.format("search gif user=%s, query=%s, isForce=%s", id, query, isForce));
        var errors = validator.validate(query);
        if (errors.isPresent()) {
            throw new ValidationException("query: " + errors.get());
        }
        ResponseEntity<String> result;
        var gifUrl = isForce
                ? service.forceSearchGif(id, query)
                : service.searchGif(id, query);
        result = gifUrl.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
        var logMsg = gifUrl.map(url -> "Found gif=" + url).orElse("Gif not found");
        logger.info(logMsg);
        return result;
    }


    @GetMapping(value = "/cache")
    public List<GifsByQueryDto> getFSCash(@RequestParam(value = "query", required = false) String query) {
        List<GifsByQueryDto> result = new ArrayList<>();
        logger.info(String.format("Search in cash query=%s", query));
        if (query != null) {
            var errors = validator.validate(query);
            if (errors.isPresent()) {
                throw new ValidationException(errors.get());
            }
            var gifs = service.getUrlsFromFSCacheByQuery(query);
            var responce = new GifsByQueryDto(query, gifs);
            result.add(responce);
        } else {
            result = service.getAllFSCacheGifs();
        }
        return result;
    }

    @PostMapping(value = "/cache/generate")
    public GifsByQueryDto cashGenerate(String query) throws IOException {
        logger.info(String.format("Generate gif in cash query=%s,", query));
        var errors = validator.validate(query);
        if (errors.isPresent()) {
            throw new ValidationException("query: " + errors.get());
        }
        var result = service.createGifFSCache(query);
        logger.info(String.format("Generated gif url=%s", result));
        return result;
    }

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "cache")
    public void deleteFSCash() {
        logger.info("Attempt to remove cache");
        service.removeFSCash();
    }

    @GetMapping(value = "gifs")
    public List<String> getAllGifs() {
        logger.info("Get all gifs");
        return service.getAllGifUrls();
    }

    @GetMapping(value = "/user/{id}/all")
    public List<GifsByQueryDto> getAllUserGifs(@PathVariable @ValidId String id) {
        logger.info("Get all user gifs user=" + id);
        return service.getUserGifs(id);
    }

    @GetMapping(value = "/user/{id}/history")
    public List<HistoryDto> getUserHistory(@PathVariable @ValidId String id) {
        logger.info("Get user history user=" + id);
        return service.getUserHistory(id);
    }

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/user/{id}/history/clean")
    public void clearUserHistory(@PathVariable @ValidId String id) {
        logger.info("Attempt to clear user history. user=" + id);
        service.clearUserHistory(id);
    }

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/user/{id}/reset")
    public void clearUserCash(@PathVariable @ValidId String id,
                              @RequestParam(required = false) String query) {
        if (query == null) {
            logger.info("Clear all user cache, user=" + id);
            service.clearUserCache(id);
        } else {
            var errors = validator.validate(query);
            if (errors.isPresent()) {
                throw new ValidationException("query: " + errors.get());
            }
            logger.info(String.format("Clear user caches by query, user=%s, query=%s", id, query));
            service.clearUserCache(id, query);
        }
    }

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/user/{id}/clean")
    public void clearUserData(@PathVariable @ValidId String id) {
        logger.info("Delete all user data, user=" + id);
        service.deleteUserData(id);
    }

}
