package com.bsa.controllers;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.bsa.config.Constants.CACHE_DIRECTORY;
import static com.bsa.config.Constants.USERS_DIRECTORY;
import static java.io.File.separator;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 05.07.2020
 */
@RestController
public class GifController {

    @GetMapping(value = "bsa_giphy/cash/{query}/{fileName:.+}")
    public void getGifFromCach(HttpServletResponse response,
                                    @PathVariable String query,
                                    @PathVariable String fileName) throws IOException {
        var filePath = CACHE_DIRECTORY + separator + query + separator + fileName;
        InputStream in = new FileInputStream(new File(filePath));
        response.setContentType(MediaType.IMAGE_GIF_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

    @GetMapping(value = "bsa_giphy/users/{userId}/{query}/{fileName:.+}")
    public void getGifFromUsers(HttpServletResponse response,
                                    @PathVariable String query,
                                    @PathVariable String userId,
                                    @PathVariable String fileName) throws IOException {
        var filePath = String.join(separator, USERS_DIRECTORY, userId, query, fileName);
        InputStream in = new FileInputStream(new File(filePath));
        response.setContentType(MediaType.IMAGE_GIF_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

}
