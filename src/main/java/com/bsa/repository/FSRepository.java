package com.bsa.repository;

import com.bsa.dto.GifDto;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.bsa.config.Constants.CACHE_DIRECTORY;
import static com.bsa.config.Constants.USERS_DIRECTORY;
import static java.io.File.separator;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 05.07.2020
 */
@Service
public class FSRepository {
    @Value("${CONNECT_TIMEOUT}")
    private int CONNECT_TIMEOUT;

    @Value("${READ_TIMEOUT}")
    private int READ_TIMEOUT;

    @Value("${server.domen}")
    private String serverPort;

    public String saveToCache(GifDto gif, String query) throws IOException {
        String filePath = String.join(separator, CACHE_DIRECTORY, query, gif.getName());
        FileUtils.copyURLToFile(gif.getUrl(), new File(filePath), CONNECT_TIMEOUT, READ_TIMEOUT);
        return this.pathToUrl(filePath);
    }

    public String saveToUsers(GifDto gif, String userId, String query) throws IOException {
        String filePath = String.join(separator,
                USERS_DIRECTORY,
                userId,
                query,
                gif.getName());
        FileUtils.copyURLToFile(gif.getUrl(), new File(filePath), CONNECT_TIMEOUT, READ_TIMEOUT);
        return this.pathToUrl(filePath);
    }

    public List<String> getFromCashByQuery(String query) {
        return this.findGifsByQuery(CACHE_DIRECTORY, query);
    }

    public List<String> getUserURLsByQuery(String user, String query) {
        List<String> result = new ArrayList<>();
        File usersDir = new File(USERS_DIRECTORY);
        var userFolder = usersDir.listFiles(f -> f.getName().equals(user));
        if (userFolder != null && userFolder.length != 0) {
            result = this.findGifsByQuery(userFolder[0].toString(), query);
        }
        return result;
    }

    private List<String> findGifsByQuery(String directory, String query) {
        List<String> result = new ArrayList<>();
        File parentDir = new File(directory);
        var queryFolder = parentDir.listFiles(f -> f.getName().equals(query));
        if (queryFolder != null && queryFolder.length != 0) {
            result = this.getUrlsFromFolder(Arrays.stream(Objects.requireNonNull(queryFolder[0].listFiles())));
        }
        return result;
    }

    private List<String> getUrlsFromFolder(Stream<File> folder) {
        return folder.map(File::toString)
                .filter(file -> file.endsWith(".gif"))
                .map(this::pathToUrl)
                .collect(Collectors.toList());
    }

    private String pathToUrl(String path) {
        return serverPort + path.replaceAll("\\\\", "/");
    }

    public List<String> getAllFSCashQueries() {
        return this.getNestedFolders(CACHE_DIRECTORY);
    }

    public List<String> getUserList() {
        return this.getNestedFolders(USERS_DIRECTORY);
    }

    public List<String> getUserQueries(String userId) {
        return this.getNestedFolders(this.getUserFolder(userId));
    }

    private List<String> getNestedFolders(String folder) {
        List<String> result = new ArrayList<>();
        var parentDir = new File(folder);
        var queries = parentDir.listFiles();
        if (queries != null) {
            result = Arrays.stream(queries).filter(File::isDirectory)
                    .map(File::getName)
                    .collect(Collectors.toList());
        }
        return result;
    }

    public void removeCache() {
        var cash = new File(CACHE_DIRECTORY);
        var folders = cash.listFiles();
        if (folders != null) {
            Arrays.stream(folders)
                    .filter(File::isDirectory)
                    .forEach(FileSystemUtils::deleteRecursively);
        }
    }

    public Optional<String> getUserHistoryFile(String userId) {
        Optional<String> result = Optional.empty();
        var userPath = getUserFolder(userId);
        var userFolder = new File(userPath);
        var history = userFolder.listFiles(file -> file.getName().equals("history.csv"));
        if(history != null && history.length > 0) {
            result = Optional.of(history[0].getAbsolutePath());
        }
        return result;
    }

    public void clearUserHistory(String userId) {
        var path = this.getUserHistoryFile(userId);
        if (path.isPresent()) {
            var file = new File(path.get());
            file.delete();
        }
    }

    public void deleteUserData(String userId) {
        var path = this.getUserFolder(userId);
        var userFolder = new File(path);
        FileSystemUtils.deleteRecursively(userFolder);
    }

    public static void main(String[] args) {
        var a = new FSRepository();
        var file = a.getUserHistoryFile("goal");
        System.out.println(file);
    }

    private String getUserFolder(String userId) {
        return USERS_DIRECTORY + separator + userId;
    }
}
