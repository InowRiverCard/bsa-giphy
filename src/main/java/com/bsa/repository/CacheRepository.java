package com.bsa.repository;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 06.07.2020
 */
@Service
public class CacheRepository {
    private final Map<String, Map<String, Set<String>>> cache = new ConcurrentHashMap<>();

    public List<String> getGifsByQuery(String userId, String query) {
        List<String> result = new ArrayList<>();
        var userGifs = cache.get(userId);
        if (userGifs != null) {
            var gifs = userGifs.get(query);
            if (!gifs.isEmpty()) {
                result = new ArrayList<>(gifs);
            }
        }
        return result;
    }

    public void putToHash(String userId, String query, String url) {
        var userFolder = cache.get(userId);
        if (userFolder == null) {
            var queryFolder = new ConcurrentHashMap<String, Set<String>>();
            var gifs = new HashSet<String>();
            gifs.add(url);
            queryFolder.put(query, gifs);
            cache.putIfAbsent(userId, queryFolder);
        } else {
            var queryFolder = userFolder.get(query);
            if (queryFolder == null) {
                var gifs = new HashSet<String>();
                gifs.add(url);
                userFolder.put(query, gifs);
            } else {
                queryFolder.add(url);
            }
        }
    }

    public void clear(String userId, String query) {
        var userFolder = this.cache.get(userId);
        if (userFolder != null) {
            userFolder.remove(query);
        }
    }

    public void clear(String userId) {
        this.cache.remove(userId);
    }

}
