package com.bsa.utils;

import com.bsa.dto.HistoryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
@Service
public class CSVReader {
    private static final Logger logger = LoggerFactory.getLogger(CSVReader.class);

    public List<HistoryDto> readFile(String fileName) {
        String line;
        String cvsSplitBy = ", ";
        List<HistoryDto> result = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] notice = line.split(cvsSplitBy);
                result.add(new HistoryDto(notice[0], notice[1], notice[2]));
            }
        } catch (IOException e) {
            logger.info("Attempt to read non existing file " + fileName);
        }
        return result;
    }

}
