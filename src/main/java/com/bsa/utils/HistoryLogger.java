package com.bsa.utils;

import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.bsa.config.Constants.USERS_DIRECTORY;
import static java.io.File.separator;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 06.07.2020
 */
@Service
public class HistoryLogger {
    private final static String HISTORY_FILE = "history.csv";

    public void log(String query, String userId, String url) throws IOException {
        var date = this.getCurrentDate();
        var filePath = this.getHistoryFilePath(userId);
        try(FileWriter fileWriter = new FileWriter(filePath, StandardCharsets.UTF_8, true);
            PrintWriter printWriter = new PrintWriter(fileWriter)){
            printWriter.println(String.format("%s, %s, %s", date, query, url));
        }

    }

    private String getHistoryFilePath(String userId) {
        return String.join(separator,
                USERS_DIRECTORY,
                userId,
                HISTORY_FILE);
    }

    private String getCurrentDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

}
