package com.bsa.validation;

import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.bsa.config.Constants.ILLEGAL_CHARACTERS;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
@Service
public class FolderNameValidator {
    public Optional<String> validate(String id) {

        Pattern ptrn = Pattern.compile("^[^\\\\<>./:|`]*$");
        Matcher matcher = ptrn.matcher(id);
        if (!matcher.matches()) {
            return Optional.of("this property can`t store such symbols " + ILLEGAL_CHARACTERS);
        }

        if (id.length() > 100) {
            return Optional.of("this property can`t be longer then 100 characters");
        }
        return Optional.empty();
    }
}
