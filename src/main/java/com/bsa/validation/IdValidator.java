package com.bsa.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.bsa.config.Constants.ILLEGAL_CHARACTERS;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
public class IdValidator implements ConstraintValidator<ValidId, String> {

    @Override
    public void initialize(ValidId validId) {

    }

    @Override
    public boolean isValid(String id, ConstraintValidatorContext ctx) {
        Pattern ptrn = Pattern.compile("^[^\\\\<>./:|`]*$");
        Matcher matcher = ptrn.matcher(id);
        if (!matcher.matches()) {
            ctx.disableDefaultConstraintViolation();
            ctx.buildConstraintViolationWithTemplate(
                    "id can`t store such symbols " + ILLEGAL_CHARACTERS)
                    .addConstraintViolation();
            return false;
        }

        if (id.length() > 100) {
            ctx.disableDefaultConstraintViolation();
            ctx.buildConstraintViolationWithTemplate(
                    "id can`t be longer then 100 characters")
                    .addConstraintViolation();
            return false;
        }

        return true;
    }
}
