package com.bsa.validation;

import java.lang.annotation.*;
import javax.validation.*;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
@Documented
@Constraint(validatedBy = IdValidator.class)
@Target( { ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidId {
    String message() default "wrong id";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
