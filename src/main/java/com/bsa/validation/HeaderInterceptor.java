package com.bsa.validation;

import com.bsa.exeption.ValidationException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
public class HeaderInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        if(!request.getRequestURI().endsWith(".gif") && request.getHeader("X-BSA-GIPHY") == null) {
            throw new ValidationException("Header \"X-BSA-GIPHY\" is necessary");
        }

        return super.preHandle(request, response, handler);
    }

}