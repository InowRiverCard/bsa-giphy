package com.bsa.exeption;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
public class ValidationException extends RuntimeException {
    public ValidationException(String msg) {
        super(msg);
    }
}
