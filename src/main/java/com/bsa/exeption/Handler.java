package com.bsa.exeption;

import com.bsa.controllers.ApiController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 07.07.2020
 */
@ControllerAdvice
public class Handler {
    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleWebExchangeBindException(ConstraintViolationException exception) {
        var errors = exception.getConstraintViolations().stream()
                .map(constr -> constr.getInvalidValue() + " " + constr.getMessage())
                .collect(Collectors.toList());
        logger.info(exception.getMessage());
        return ResponseEntity.badRequest().body(errors.toString());
    }


    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handleWebExchangeBindException(ValidationException exception) {
        logger.info(exception.getMessage());
        return ResponseEntity.badRequest().body(exception.getMessage());
    }
}
