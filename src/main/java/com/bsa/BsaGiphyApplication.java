package com.bsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@PropertySource("classpath:dev.properties")
@SpringBootApplication
public class BsaGiphyApplication  {
	public static void main(String[] args) {
		SpringApplication.run(BsaGiphyApplication.class, args);
	}
}
